
## Dockerfile

Inicialmente eu tentei criar a imagem com a versão 17 do node
mas no final das contas acorreram vários erros por questão de dependências e a versão que foi escolhida no final foi a 12
### Pipeline gitlab

logo após a criação da pipeline eu criei as váriaveis do
ambiente de CI para fazer a criação e o push da imagem para o docker hub utilizando a propria imagem do docker na stage do pipeline.

a implementação da pipeline para subir no docker hub foi bem simples.

### Deploy no Heroku

com certeza essa parte foi a que me deu mais dor de cabeça, no final das contas depois de resolver um problema atrás do outro
e conseguir subir a imagem para o registry do heroku, ao tentar realizar o release dessa imagem é necessário estar autorizado e eu não conseguir implementar a autorização por meio do ambiente de CI para que o container fosse upado.
Além disso provavelmente outros problemas ocorreriam por exemplo
o fato do heroku assinalar uma porta aleatoria para a aplicação
assim não funcionando com a porta padrão que o node usa para usar
o React.

De qualquer forma eu consegui deployar na mão, segue o link https://desafio-devops-docker.herokuapp.com/

